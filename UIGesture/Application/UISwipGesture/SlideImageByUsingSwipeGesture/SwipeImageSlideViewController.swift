//
//  SwipeImageSlideViewController.swift
//  UIGesture
//
//  Created by Phanna on 2/1/23.
//

import UIKit

class SwipeImageSlideViewController: UIViewController {

    @IBOutlet weak var imageSlide: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageSlide.isUserInteractionEnabled = true
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.actionImageSlide))
        swipeRight.direction = .right
        imageSlide.addGestureRecognizer(swipeRight)
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.actionImageSlide))
        swipeLeft.direction = .left
        imageSlide.addGestureRecognizer(swipeLeft)
        // Do any additional setup after loading the view.
    }
    
    @objc func actionImageSlide(sender: UISwipeGestureRecognizer?){
        if let swipeGesture = sender {
            switch swipeGesture.direction{
                
            case .right:
                imageSlide.image = UIImage(named: "all")
                print("I just swipe right")
            case .left:
                imageSlide.image = UIImage(named: "ip14")
                print("I just swipe left")
            default:
                break
            }
        }
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
