//
//  SwipeGestureViewCell.swift
//  UIGesture
//
//  Created by joseph on 26/12/22.
//

import UIKit

class SwipeGestureViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel   : UILabel!
    
    @IBOutlet weak var viewGesture: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
