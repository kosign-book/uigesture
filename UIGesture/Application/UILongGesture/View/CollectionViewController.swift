//
//  CollectionViewController.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//

import UIKit

class CollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var indexCompare    : IndexPath?
    var collectionCellVM = CollectionCellVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        collectionView?.addGestureRecognizer(gesture)
        self.collectionCellVM.CollectionCellData()
        collectionView!.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleLongPress(_ gesture: UILongPressGestureRecognizer) {
        guard let collectionView = collectionView else {
            return
        }
        switch gesture.state {
        case .began:
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                collectionView.cancelInteractiveMovement()
                return
            }
            collectionView.beginInteractiveMovementForItem(at: targetIndexPath)
            
        case .changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: collectionView))
            
        case .ended:
            collectionView.endInteractiveMovement()
            guard let targetIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                return
            }
            //reset color when item not change
            if indexCompare?.row == targetIndexPath.row {
                resetColorItem(indexPath: indexCompare ?? IndexPath(item: 0, section: 0))
            }
            
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
    
    func resetColorItem (indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ItemCellCollectionViewCell
        cell.view.layer.shadowColor    = UIColor.gray.cgColor
    }
}

extension CollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 4
        let totalSpace = collectionView.contentInset.left
        + collectionView.contentInset.right
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
         return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        //when we long gesture on collection and remove it
        let item = collectionCellVM.collection.remove(at: sourceIndexPath.row)
        //when we unlong gesture and start insert to destination
        collectionCellVM.collection.insert(item, at: destinationIndexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCellCollectionViewCell", for: indexPath) as! ItemCellCollectionViewCell
        cell.lblNum.text = collectionCellVM.collection[indexPath.row].name
        //cell.config(data: collectionCellVM.collection[indexPath.row].name)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionCellVM.collection.count
    }
    
}

