//
//  ItemCellCollectionViewCell.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//

import UIKit

class ItemCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblNum: UILabel!
    
    func config(data: CollectionCellModel){
        self.lblNum.text = data.name
    }
}
