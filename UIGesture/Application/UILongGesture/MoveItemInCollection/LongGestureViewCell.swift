//
//  LongGestureViewCell.swift
//  UIGesture
//
//  Created by joseph on 26/12/22.
//

import UIKit

class LongGestureViewCell: UITableViewCell {

    @IBOutlet weak var viewGesture: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
