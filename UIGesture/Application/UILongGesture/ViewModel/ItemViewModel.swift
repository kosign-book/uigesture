//
//  ViewModel.swift
//  UIGesture
//
//  Created by Phanna on 1/1/23.
//

import Foundation

class CollectionCellVM {
    var collection: [CollectionCellModel] = []
    func CollectionCellData(){
        collection = [
            CollectionCellModel(name: "A"),
            CollectionCellModel(name: "B"),
            CollectionCellModel(name: "C"),
            CollectionCellModel(name: "D"),
            CollectionCellModel(name: "E"),
            CollectionCellModel(name: "F"),
            CollectionCellModel(name: "G"),
            CollectionCellModel(name: "H"),
            CollectionCellModel(name: "I"),
            CollectionCellModel(name: "J"),
            CollectionCellModel(name: "K"),
            CollectionCellModel(name: "L"),
            CollectionCellModel(name: "M"),
            CollectionCellModel(name: "N"),
            CollectionCellModel(name: "O"),
            CollectionCellModel(name: "P"),
            CollectionCellModel(name: "Q"),
            CollectionCellModel(name: "R"),
            CollectionCellModel(name: "S"),
            CollectionCellModel(name: "T"),
            CollectionCellModel(name: "U"),
            CollectionCellModel(name: "V"),
            CollectionCellModel(name: "W"),
            CollectionCellModel(name: "X"),
            CollectionCellModel(name: "Y"),
            CollectionCellModel(name: "Z"),
        ]
        
    }
}
