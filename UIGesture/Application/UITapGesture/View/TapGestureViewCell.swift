//
//  TapGestureViewCell.swift
//  UIGesture
//
//  Created by joseph on 26/12/22.
//

import UIKit

class TapGestureViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var viewGesture: UIView!
    
    var owner: UIViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewGesture.isUserInteractionEnabled = true
        viewGesture.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:))))
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        print("image tapped")
    }
//
//    @objc func tap(sender: UITapGestureRecognizer){
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "AlertPopUpViewController")
//        self.present(vc, animated: true)
//    }


}
