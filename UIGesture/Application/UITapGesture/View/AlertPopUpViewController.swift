//
//  AlertPopUpViewController.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//

import UIKit

class AlertPopUpViewController: UIViewController {

    @IBOutlet weak var image        : UIImageView!
    @IBOutlet weak var imageSmall   : UIImageView!
    
    var isCheck :Bool = true
    var isLiked = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func TapGestureAction(_ sender: Any) {

        isCheck = !isCheck
        if isCheck {
            imageSmall.image = UIImage(systemName: "")
            if !isCheck{
                imageSmall.image = UIImage(systemName: "bolt.heart.fill")
            }
            
        } else {
            imageSmall.image = UIImage(systemName: "bolt.heart.fill")
           
        }
        
        
    }
    
   


}
