//
//  Viewmodel.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//

import Foundation

class GestureVM {
    var dataCell = [GestureCellModel]()
    
    func gestureData(){
        dataCell = [
            GestureCellModel(name: "TapGesture",    type: .Tap),
            GestureCellModel(name: "SwipGesture",   type: .Swipe),
            GestureCellModel(name: "PanGesture",    type: .Pan),
            GestureCellModel(name: "LongGesture",   type: .Long)
        ]
    }
}

