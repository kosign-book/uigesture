//
//  Model.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//


import Foundation


    
enum Type: String {
    case Tap
    case Swipe
    case Pan
    case Long
}


struct GestureCellModel {
    let name : String?
//    let color   : String?
    let type:Type?
}

