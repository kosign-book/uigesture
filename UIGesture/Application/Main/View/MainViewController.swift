//
//  MainViewController.swift
//  UIGesture
//
//  Created by Phanna on 2/10/22.
//

import UIKit

class MainViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var fileViewOrigin: CGPoint!
    var gestureVM = GestureVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureVM.gestureData()
    }
    
    // -Tap gesture function
    @objc func tap(sender: UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "AlertPopUpSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlertPopUpViewController")
        self.present(vc, animated: true)
    }
    // -Swip gesture function
    @objc func swipe(sender: UISwipeGestureRecognizer){
        let storyboard = UIStoryboard(name: "SwipeImageSlideSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SwipeImageSlideViewController")
        self.present(vc, animated: true)
    }
    // -handle gesture function
    @objc func handlePan(recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    // -Long gesture function
    @objc func longPress(sender: UILongPressGestureRecognizer){
        let storyboard = UIStoryboard(name: "CollectionViewSB", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CollectionViewController")
        vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
        self.present(vc, animated: true)
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gestureVM.dataCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = gestureVM.dataCell[indexPath.row].type
        switch type {
            
        case .Tap:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TapGestureViewCell", for: indexPath) as! TapGestureViewCell
            cell.titleLabel.text             = "TapGesture"
            
            print("hello")
            let TapGestureReconizers = UITapGestureRecognizer(target: self.self, action: #selector(tap))
            TapGestureReconizers.numberOfTapsRequired       = 1
            TapGestureReconizers.numberOfTouchesRequired    = 1
            
            cell.viewGesture.addGestureRecognizer(TapGestureReconizers)
            cell.viewGesture.isUserInteractionEnabled = true
            
            
            return cell
            
        case .Swipe:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SwipeGestureViewCell", for: indexPath) as! SwipeGestureViewCell
            cell.titleLabel.text        = "SwipGesture"
            let gestureReconizerr       = UISwipeGestureRecognizer(target: self, action: #selector(swipe))
            
            // -direction we swipe
            gestureReconizerr.direction = .right
            
            // -one finger for tap if 2 two finger can tap
            gestureReconizerr.numberOfTouchesRequired = 1
            
            cell.viewGesture.addGestureRecognizer(gestureReconizerr)
            cell.viewGesture.isUserInteractionEnabled = true
            
            return cell
            
        case .Pan:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PanGestureViewCell", for: indexPath) as! PanGestureViewCell
            cell.lblTapGesture.text      = "PanGesture"
            let pan                      = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
            
            cell.bkgView.addGestureRecognizer(pan)
            cell.bkgView.isUserInteractionEnabled = true
            
            return cell
            
        case .Long:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LongGestureViewCell", for: indexPath) as! LongGestureViewCell
            cell.titleLabel.text             = "LongGesture"
            let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
            longpress.minimumPressDuration = 0.5
            
            cell.viewGesture.addGestureRecognizer(longpress)
            cell.viewGesture.isUserInteractionEnabled = true
            return cell
            
        case .none:
            break
        }
        return UITableViewCell()
    }
}
