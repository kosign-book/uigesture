//
//  Shared.swift
//  KOSIGN_LMS
//
//  Created by lymanny on 16/3/22.
//

import Foundation
import UIKit

enum LanguageCode: String {
    case Korean     = "ko"
    case English    = "en"
    case Khmer      = "km"
}


struct Shared {
    
    //MARK:- singleton
    static var share = Shared()
    
    private init() { }
    
    static var language                 : LanguageCode = .English
    var jSessionId                      : String?

    
    //MARK:- singleton
//    static let share                    = ShareProperty()
//    static var language : LanguageCode  = .Khmer
    
    
    
    func getLocalizedFont() -> String {
        
        var fontName = "Helvetica Neue Medium Extended"
               
        if Shared.language.rawValue == LanguageCode.Khmer.rawValue {
            fontName = "Hanuman-Regular"
        } else {
            fontName = "Helvetica Neue Medium Extended"
        }
        return fontName
    }
    
    var height1 :CGFloat = 500
    
    static var safeAreaBottom : CGFloat {
        let root = UIApplication.shared.keyWindow?.rootViewController
        
        var safeAreaBottom  : CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            safeAreaBottom  = root?.view.safeAreaInsets.bottom ?? 0.0
        } else {
            // Fallback on earlier versions
        }
        
        return safeAreaBottom
    }

}

